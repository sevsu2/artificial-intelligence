
(defvar openList)
(defvar closedList)

(defvar startStatement '((1 2 3) (4 5 8) (6 7 -1)))
(defvar endStatement '((1 2 3) (4 5 6) (7 8 -1)))
(defvar emptyCoords '(1 1))

(defvar pointers)

(defun AppendToOpen(elem)
	(setq reverseL (reverse openList))
	(push elem reverseL)
	(setf openList (reverse reverseL))
)

(defun AppendToclosed(elem)
	(setq reverseL (reverse closedList))
	(push elem reverseL)
	(setf closedList (reverse reverseL))
)

(defun RemoveFromOpen()
	(pop openList)
)

(defun RemoveFromClosed()
	(pop closedList)
)

; Move end of first list to second list
(defun MoveFromOpenToClosed()
	(setq elem (RemoveFromOpen))
	(AppendToclosed elem)
)

(defun PrintStatement(statement)
	(dolist (elem statement)
		(print elem)
	)
	(print "")
)

(defun PrintList(l)
	(dolist (elem l)
		(PrintStatement elem)
	)
	(print "   ")
)

(defun IsStatementMember(statement lst)
	(dolist (currentSt lst)
		(setf isFind T)
		(loop for i from 0 to 2 do 
			(if (not (equal (nth i currentSt) (nth i statement))) (setf isFind NIL))
		)

		(if isFind (return-from IsStatementMember T))
	)

	(return-from IsStatementMember NIL)
)

(defun BreadFirst()

	; Move start element to OPEN
	(setf openList (list startStatement))
	(setf pointers (pairlis (list startStatement) '(1)))

	;CLOSED = empty list
	(setf closedList (list))

	; while OPEN is not empty  do
	(loop while (not (null openList)) do

		;Get first element from OPEN = firstEl
		(setf firstEl (first openList))
		(PrintList openList)
		; (read)

		(setf emptyCoords (FindEmptyCell firstEl))

		; (print emptyCoords)
		; (PrintStatement firstEl)

		;if first element is target point - return true
		(if (equal firstEl endStatement) (return-from BreadFirst (GetSolutionPath firstEl)))

		;Otherwise move first element to CLOSED
		(MoveFromOpenToClosed)

		;Apply state function to first element. 
		(setf newStatements (GetNextStatements firstEl))

		(dolist (elem newStatements)


			;All childs move to end of OPEN. 
			(if (and (not (IsStatementMember elem openList)) (not (IsStatementMember elem closedList))) (AppendToOpen elem))

			;Bind childs and our first element with pointer
			(setf pointers (acons elem (list firstEl) pointers))
		)		
	)

	(return-from BreadFirst NIL)
)


(defun FindEmptyCell(statement)
	(setf row 0)
	(setf col 0)

	(dolist (elem statement)

		;Find empty cell
		(setf emptyIndex (position -1 elem :test #'equal))

		;Calculate index of empty cell
		(if (not (null emptyIndex)) (return-from FindEmptyCell (list row emptyIndex)))
		(incf row)
	)

	(return-from FindEmptyCell NIL)
)

(defun GetNextStatements(statement)
	(setf resultStatements (list))

	;Get up statement
	(setf upIndex (- (first emptyCoords) 1))
	(if (>= upIndex 0) 
		(push (CalculateStatement statement (list upIndex (second emptyCoords))) resultStatements)
	)

	;Get down statement
	(setf downIndex (+ (first emptyCoords) 1))
	(if (< downIndex 3) 
		(push (CalculateStatement statement (list downIndex (second emptyCoords))) resultStatements)
	)

	;Get left statement
	(setf leftIndex (- (second emptyCoords) 1))
	(if (>= leftIndex 0)
		(push (CalculateStatement statement (list (first emptyCoords) leftIndex)) resultStatements)
	)

	;Get right statement
	(setf rightIndex (+ (second emptyCoords) 1))
	(if (< rightIndex 3) 
		(push (CalculateStatement statement (list (first emptyCoords) rightIndex)) resultStatements)
	)

	(return-from GetNextStatements resultStatements)
)


(defun CalculateStatement(statement swapCoords)
	(setf newStatement (copy-tree statement))
	
	; (print swapCoords)

	(setf temp (nth (second swapCoords) (nth (first swapCoords) newStatement)))
	(setf (nth (second swapCoords) (nth (first swapCoords) newStatement)) (nth (second emptyCoords) (nth (first emptyCoords) newStatement)))
	(setf (nth (second emptyCoords) (nth (first emptyCoords) newStatement)) temp)

	(return-from CalculateStatement newStatement)
)

(defun GetSolutionPath(element)
	(setf temp element)
	(setf path (list))

	(loop while (not (equal (cdr (assoc temp pointers)) 1)) do
		(push temp path)
		(setf temp (second (assoc temp pointers)))
	)

	(push temp path)

	(return-from GetSolutionPath path)
)

(print (BreadFirst))
; (print openList)
; (print closedList)